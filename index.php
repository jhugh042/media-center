<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<style>
	body{
		background-color:gray;
	}
	.row{
		position:absolute;
		top:0;
		left:0;
		width:100%;
		height:50%;
	}
	
    .container{
        width:23%;
        height:100%;
		margin:1%;
		float:left;
		box-shadow: 10px 10px;
    }
	
	.container:hover{
		transform: scale(1.1,1.1);
	}
    .container img{
		width:100%;
		height:100%;
		object-fit: cover;
    }
	
	@media all and (max-width:980px){
		.container{
			width:31%;
		}
	}
	@media all and (max-width:740px){
		.container{
			width:48%;
		}
	}
	@media all and (max-width:485px){
		.container{
			width:100%;
		}
	}
</style>
<body>
	<div class="row">
		<?php
			$dir="./";
			$array = scandir($dir);
			foreach($array as $value){
				if(is_dir($value) and $value<>"." and $value<>".."){
					$name= str_replace('_', ' ', $value);
					echo "<div class=\"container\">";
					echo "<a href=\"video.html?video=".$value."\">";
					echo "<img src=\"".$value.".jpg\" alt=\"".$name."\"></a>";
					echo "</div>";
				}
			}
		?>
	</div>
</body>
</html>

