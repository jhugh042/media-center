// Video
var videoCont = document.getElementById("video-container");
var video = document.getElementById("video");

// Buttons
var next = document.getElementById("next");
var playButton = document.getElementById("play-pause");
var muteButton = document.getElementById("mute");
var fullScreenButton = document.getElementById("full-screen");

// Sliders
var seekBar = document.getElementById("seek-bar");
var volumeBar = document.getElementById("volume-bar");
        volumeBar.value = 10;
        video.volume = 0.1;
var controls = document.getElementById("video-controls");
//backend
var base="/Movies/" + getUrlVars()["video"]+ "/E";
video.src = base+cookieControl()+".mp4";
document.getElementById("text").value = cookieControl();

window.onload = function() {

	//event listener for key presses
	window.addEventListener("keydown",handleKeyPress);
	
	// Event listener for the next button
	next.addEventListener("click", nextButtonPress);

	// Event listener for the play/pause button
	playButton.addEventListener("click", playButtonPress);

	// Event listener for the mute button
	muteButton.addEventListener("click", muteButtonPress);

	// Event listener for the full-screen button
	fullScreenButton.addEventListener("click", fullScreenButtonPress);

	// Event listener for the seek bar
	seekBar.addEventListener("change", function() {
		// Calculate the new time
		var time = video.duration * (seekBar.value / 100);
		// Update the video time
		video.currentTime = time;
	});
	// Update the seek bar as the video plays
	video.addEventListener("timeupdate", function() {
		// Calculate the slider value
		var value = (100 / video.duration) * video.currentTime;
		// Update the slider value
		seekBar.value = value;
	});
	// Pause/play on click
	video.addEventListener("click", function() {
		if (video.paused == true) {
			// Play the video
			video.play();
			// Update the button text to 'Pause'
			playButton.innerHTML = "&#10074;&#10074;";
		} else {
			// Pause the video
			video.pause();
			// Update the button text to 'Play'
			playButton.innerHTML = "&#9658;";
		}
	});
	// Pause the video when the seek handle is being dragged
	seekBar.addEventListener("mousedown", function() {
		video.pause();
	});
	// Play the video when the seek handle is dropped
	seekBar.addEventListener("mouseup", function() {
		video.play();
		playButton.innerHTML = "&#10074;&#10074;";
	});
	// Event listener for the volume bar
	volumeBar.addEventListener("change", function() {
		// Update the video volume
		video.volume = volumeBar.value/100;
	});
}
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}
function cookieControl(){
	var episode = getCookie(getUrlVars()["video"]);
	if(episode != ""){
		return episode;
	}else{
		setCookie(getUrlVars()["video"],"001",30);
		return "001";
	}
}
function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}
function setCookie(cname,cvalue,exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires=" + d.toGMTString();
	document.cookie = cname + "=" + cvalue + ";" + expires;
}
function handleKeyPress(e){
	switch(e.keyCode){
		case 37://left
			var value = video.currentTime - ( video.duration/100);
			video.currentTime = value;
			break;
		case 38://up
			if((video.volume+0.05)>=1){
				video.volume=1;
			}else{
				video.volume = video.volume+0.05;
			}
			volumeBar.value = video.volume*100;
			break;
		case 39://right
			var value = ( video.duration/ 100) + video.currentTime;
			video.currentTime = value;
			break;
		case 40://down
			if((video.volume-0.05)<=0){
				video.volume=0;
			}else{
				video.volume = video.volume-0.05;
			}
			volumeBar.value = video.volume*100;
			break;
		case 32: //space
			if (video.paused == true) {
				video.play();
				playButton.innerHTML = "&#10074;&#10074;";
			} else {
				video.pause();
				playButton.innerHTML = "&#9658;";
			}
			break;
		case 78://n key
			nextButtonPress();
			break;
	}
}
function nextButtonPress(){
	var num = document.getElementById("text").value;
	var integer = parseInt(num,10) + 1;
	if(integer<10){
		var file = base+"00"+(integer)+".mp4";
		document.getElementById("text").value = (integer);
		setCookie(getUrlVars()["video"],"00"+(integer),30);
	}else if(integer<100){
		var file = base+"0"+(integer)+".mp4";
		document.getElementById("text").value = (integer);
		setCookie(getUrlVars()["video"],"0"+(integer),30);
	}else{
		var file = base+(integer)+".mp4";
		document.getElementById("text").value = (integer);
		setCookie(getUrlVars()["video"],(integer),30);
	}
	document.getElementById("video").src = file;
}
function playButtonPress(){
	if (video.paused == true) {
		// Play the video
		video.play();
		// Update the button text to 'Pause'
		playButton.innerHTML = "&#10074;&#10074;";
	} else {
		// Pause the video
		video.pause();
		// Update the button text to 'Play'
		playButton.innerHTML = "&#9658;";
	}
}
function muteButtonPress(){
	if (video.muted == false) {
		// Mute the video
		video.muted = true;
		// Update the button text
		muteButton.innerHTML = "&#128264;"; //one sound = &#128265;
	} else {
		// Unmute the video
		video.muted = false;
		// Update the button text
		muteButton.innerHTML = "&#128266;";
	}
}
function fullScreenButtonPress(){
	if (videoCont.requestFullscreen) {
		videoCont.requestFullscreen();
	} else if (videoCont.mozRequestFullScreen) {
		videoCont.mozRequestFullScreen(); // Firefox
	} else if (videoCont.webkitRequestFullscreen) {
		videoCont.webkitRequestFullscreen(); // Chrome and Safari
	}
}

